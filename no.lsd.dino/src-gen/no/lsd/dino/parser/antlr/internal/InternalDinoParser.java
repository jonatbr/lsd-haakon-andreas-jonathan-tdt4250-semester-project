package no.lsd.dino.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import no.lsd.dino.services.DinoGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDinoParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Dinosaur'", "'childOf'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=5;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_INT=6;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalDinoParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDinoParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDinoParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDino.g"; }



     	private DinoGrammarAccess grammarAccess;

        public InternalDinoParser(TokenStream input, DinoGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Model";
       	}

       	@Override
       	protected DinoGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModel"
    // InternalDino.g:64:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalDino.g:64:46: (iv_ruleModel= ruleModel EOF )
            // InternalDino.g:65:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalDino.g:71:1: ruleModel returns [EObject current=null] : ( (lv_dinosaur_0_0= ruleDinosaur ) )* ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        EObject lv_dinosaur_0_0 = null;



        	enterRule();

        try {
            // InternalDino.g:77:2: ( ( (lv_dinosaur_0_0= ruleDinosaur ) )* )
            // InternalDino.g:78:2: ( (lv_dinosaur_0_0= ruleDinosaur ) )*
            {
            // InternalDino.g:78:2: ( (lv_dinosaur_0_0= ruleDinosaur ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalDino.g:79:3: (lv_dinosaur_0_0= ruleDinosaur )
            	    {
            	    // InternalDino.g:79:3: (lv_dinosaur_0_0= ruleDinosaur )
            	    // InternalDino.g:80:4: lv_dinosaur_0_0= ruleDinosaur
            	    {

            	    				newCompositeNode(grammarAccess.getModelAccess().getDinosaurDinosaurParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_dinosaur_0_0=ruleDinosaur();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getModelRule());
            	    				}
            	    				add(
            	    					current,
            	    					"dinosaur",
            	    					lv_dinosaur_0_0,
            	    					"no.lsd.dino.Dino.Dinosaur");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleDinosaur"
    // InternalDino.g:100:1: entryRuleDinosaur returns [EObject current=null] : iv_ruleDinosaur= ruleDinosaur EOF ;
    public final EObject entryRuleDinosaur() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDinosaur = null;


        try {
            // InternalDino.g:100:49: (iv_ruleDinosaur= ruleDinosaur EOF )
            // InternalDino.g:101:2: iv_ruleDinosaur= ruleDinosaur EOF
            {
             newCompositeNode(grammarAccess.getDinosaurRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDinosaur=ruleDinosaur();

            state._fsp--;

             current =iv_ruleDinosaur; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDinosaur"


    // $ANTLR start "ruleDinosaur"
    // InternalDino.g:107:1: ruleDinosaur returns [EObject current=null] : (otherlv_0= 'Dinosaur' ( (lv_id_1_0= RULE_ID ) ) ( (lv_name_2_0= RULE_STRING ) ) ( (lv_age_3_0= RULE_INT ) ) (otherlv_4= 'childOf' ( (otherlv_5= RULE_ID ) ) )? ) ;
    public final EObject ruleDinosaur() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        Token lv_name_2_0=null;
        Token lv_age_3_0=null;
        Token otherlv_4=null;
        Token otherlv_5=null;


        	enterRule();

        try {
            // InternalDino.g:113:2: ( (otherlv_0= 'Dinosaur' ( (lv_id_1_0= RULE_ID ) ) ( (lv_name_2_0= RULE_STRING ) ) ( (lv_age_3_0= RULE_INT ) ) (otherlv_4= 'childOf' ( (otherlv_5= RULE_ID ) ) )? ) )
            // InternalDino.g:114:2: (otherlv_0= 'Dinosaur' ( (lv_id_1_0= RULE_ID ) ) ( (lv_name_2_0= RULE_STRING ) ) ( (lv_age_3_0= RULE_INT ) ) (otherlv_4= 'childOf' ( (otherlv_5= RULE_ID ) ) )? )
            {
            // InternalDino.g:114:2: (otherlv_0= 'Dinosaur' ( (lv_id_1_0= RULE_ID ) ) ( (lv_name_2_0= RULE_STRING ) ) ( (lv_age_3_0= RULE_INT ) ) (otherlv_4= 'childOf' ( (otherlv_5= RULE_ID ) ) )? )
            // InternalDino.g:115:3: otherlv_0= 'Dinosaur' ( (lv_id_1_0= RULE_ID ) ) ( (lv_name_2_0= RULE_STRING ) ) ( (lv_age_3_0= RULE_INT ) ) (otherlv_4= 'childOf' ( (otherlv_5= RULE_ID ) ) )?
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getDinosaurAccess().getDinosaurKeyword_0());
            		
            // InternalDino.g:119:3: ( (lv_id_1_0= RULE_ID ) )
            // InternalDino.g:120:4: (lv_id_1_0= RULE_ID )
            {
            // InternalDino.g:120:4: (lv_id_1_0= RULE_ID )
            // InternalDino.g:121:5: lv_id_1_0= RULE_ID
            {
            lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_id_1_0, grammarAccess.getDinosaurAccess().getIdIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDinosaurRule());
            					}
            					setWithLastConsumed(
            						current,
            						"id",
            						lv_id_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalDino.g:137:3: ( (lv_name_2_0= RULE_STRING ) )
            // InternalDino.g:138:4: (lv_name_2_0= RULE_STRING )
            {
            // InternalDino.g:138:4: (lv_name_2_0= RULE_STRING )
            // InternalDino.g:139:5: lv_name_2_0= RULE_STRING
            {
            lv_name_2_0=(Token)match(input,RULE_STRING,FOLLOW_6); 

            					newLeafNode(lv_name_2_0, grammarAccess.getDinosaurAccess().getNameSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDinosaurRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalDino.g:155:3: ( (lv_age_3_0= RULE_INT ) )
            // InternalDino.g:156:4: (lv_age_3_0= RULE_INT )
            {
            // InternalDino.g:156:4: (lv_age_3_0= RULE_INT )
            // InternalDino.g:157:5: lv_age_3_0= RULE_INT
            {
            lv_age_3_0=(Token)match(input,RULE_INT,FOLLOW_7); 

            					newLeafNode(lv_age_3_0, grammarAccess.getDinosaurAccess().getAgeINTTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDinosaurRule());
            					}
            					setWithLastConsumed(
            						current,
            						"age",
            						lv_age_3_0,
            						"org.eclipse.xtext.common.Terminals.INT");
            				

            }


            }

            // InternalDino.g:173:3: (otherlv_4= 'childOf' ( (otherlv_5= RULE_ID ) ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==12) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalDino.g:174:4: otherlv_4= 'childOf' ( (otherlv_5= RULE_ID ) )
                    {
                    otherlv_4=(Token)match(input,12,FOLLOW_4); 

                    				newLeafNode(otherlv_4, grammarAccess.getDinosaurAccess().getChildOfKeyword_4_0());
                    			
                    // InternalDino.g:178:4: ( (otherlv_5= RULE_ID ) )
                    // InternalDino.g:179:5: (otherlv_5= RULE_ID )
                    {
                    // InternalDino.g:179:5: (otherlv_5= RULE_ID )
                    // InternalDino.g:180:6: otherlv_5= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getDinosaurRule());
                    						}
                    					
                    otherlv_5=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(otherlv_5, grammarAccess.getDinosaurAccess().getChildofDinosaurCrossReference_4_1_0());
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDinosaur"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000001002L});

}