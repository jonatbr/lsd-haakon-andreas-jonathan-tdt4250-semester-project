/**
 * generated by Xtext 2.25.0
 */
package no.lsd.dino.dino;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dinosaur</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.lsd.dino.dino.Dinosaur#getId <em>Id</em>}</li>
 *   <li>{@link no.lsd.dino.dino.Dinosaur#getName <em>Name</em>}</li>
 *   <li>{@link no.lsd.dino.dino.Dinosaur#getAge <em>Age</em>}</li>
 *   <li>{@link no.lsd.dino.dino.Dinosaur#getChildof <em>Childof</em>}</li>
 * </ul>
 *
 * @see no.lsd.dino.dino.DinoPackage#getDinosaur()
 * @model
 * @generated
 */
public interface Dinosaur extends EObject
{
  /**
   * Returns the value of the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' attribute.
   * @see #setId(String)
   * @see no.lsd.dino.dino.DinoPackage#getDinosaur_Id()
   * @model
   * @generated
   */
  String getId();

  /**
   * Sets the value of the '{@link no.lsd.dino.dino.Dinosaur#getId <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' attribute.
   * @see #getId()
   * @generated
   */
  void setId(String value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see no.lsd.dino.dino.DinoPackage#getDinosaur_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link no.lsd.dino.dino.Dinosaur#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Age</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Age</em>' attribute.
   * @see #setAge(int)
   * @see no.lsd.dino.dino.DinoPackage#getDinosaur_Age()
   * @model
   * @generated
   */
  int getAge();

  /**
   * Sets the value of the '{@link no.lsd.dino.dino.Dinosaur#getAge <em>Age</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Age</em>' attribute.
   * @see #getAge()
   * @generated
   */
  void setAge(int value);

  /**
   * Returns the value of the '<em><b>Childof</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Childof</em>' reference.
   * @see #setChildof(Dinosaur)
   * @see no.lsd.dino.dino.DinoPackage#getDinosaur_Childof()
   * @model
   * @generated
   */
  Dinosaur getChildof();

  /**
   * Sets the value of the '{@link no.lsd.dino.dino.Dinosaur#getChildof <em>Childof</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Childof</em>' reference.
   * @see #getChildof()
   * @generated
   */
  void setChildof(Dinosaur value);

} // Dinosaur
