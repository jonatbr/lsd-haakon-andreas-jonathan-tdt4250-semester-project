/*
 * generated by Xtext 2.25.0
 */
package no.lsd.dino.ui;

import com.google.inject.Injector;
import no.lsd.dino.ui.internal.DinoActivator;
import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class DinoExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return FrameworkUtil.getBundle(DinoActivator.class);
	}
	
	@Override
	protected Injector getInjector() {
		DinoActivator activator = DinoActivator.getInstance();
		return activator != null ? activator.getInjector(DinoActivator.NO_LSD_DINO_DINO) : null;
	}

}
