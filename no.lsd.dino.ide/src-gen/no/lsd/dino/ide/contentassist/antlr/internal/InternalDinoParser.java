package no.lsd.dino.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import no.lsd.dino.services.DinoGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDinoParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Dinosaur'", "'childOf'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=5;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_INT=6;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalDinoParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDinoParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDinoParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDino.g"; }


    	private DinoGrammarAccess grammarAccess;

    	public void setGrammarAccess(DinoGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleModel"
    // InternalDino.g:53:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // InternalDino.g:54:1: ( ruleModel EOF )
            // InternalDino.g:55:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalDino.g:62:1: ruleModel : ( ( rule__Model__DinosaurAssignment )* ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:66:2: ( ( ( rule__Model__DinosaurAssignment )* ) )
            // InternalDino.g:67:2: ( ( rule__Model__DinosaurAssignment )* )
            {
            // InternalDino.g:67:2: ( ( rule__Model__DinosaurAssignment )* )
            // InternalDino.g:68:3: ( rule__Model__DinosaurAssignment )*
            {
             before(grammarAccess.getModelAccess().getDinosaurAssignment()); 
            // InternalDino.g:69:3: ( rule__Model__DinosaurAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalDino.g:69:4: rule__Model__DinosaurAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Model__DinosaurAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getDinosaurAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleDinosaur"
    // InternalDino.g:78:1: entryRuleDinosaur : ruleDinosaur EOF ;
    public final void entryRuleDinosaur() throws RecognitionException {
        try {
            // InternalDino.g:79:1: ( ruleDinosaur EOF )
            // InternalDino.g:80:1: ruleDinosaur EOF
            {
             before(grammarAccess.getDinosaurRule()); 
            pushFollow(FOLLOW_1);
            ruleDinosaur();

            state._fsp--;

             after(grammarAccess.getDinosaurRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDinosaur"


    // $ANTLR start "ruleDinosaur"
    // InternalDino.g:87:1: ruleDinosaur : ( ( rule__Dinosaur__Group__0 ) ) ;
    public final void ruleDinosaur() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:91:2: ( ( ( rule__Dinosaur__Group__0 ) ) )
            // InternalDino.g:92:2: ( ( rule__Dinosaur__Group__0 ) )
            {
            // InternalDino.g:92:2: ( ( rule__Dinosaur__Group__0 ) )
            // InternalDino.g:93:3: ( rule__Dinosaur__Group__0 )
            {
             before(grammarAccess.getDinosaurAccess().getGroup()); 
            // InternalDino.g:94:3: ( rule__Dinosaur__Group__0 )
            // InternalDino.g:94:4: rule__Dinosaur__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Dinosaur__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDinosaurAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDinosaur"


    // $ANTLR start "rule__Dinosaur__Group__0"
    // InternalDino.g:102:1: rule__Dinosaur__Group__0 : rule__Dinosaur__Group__0__Impl rule__Dinosaur__Group__1 ;
    public final void rule__Dinosaur__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:106:1: ( rule__Dinosaur__Group__0__Impl rule__Dinosaur__Group__1 )
            // InternalDino.g:107:2: rule__Dinosaur__Group__0__Impl rule__Dinosaur__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Dinosaur__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Dinosaur__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__Group__0"


    // $ANTLR start "rule__Dinosaur__Group__0__Impl"
    // InternalDino.g:114:1: rule__Dinosaur__Group__0__Impl : ( 'Dinosaur' ) ;
    public final void rule__Dinosaur__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:118:1: ( ( 'Dinosaur' ) )
            // InternalDino.g:119:1: ( 'Dinosaur' )
            {
            // InternalDino.g:119:1: ( 'Dinosaur' )
            // InternalDino.g:120:2: 'Dinosaur'
            {
             before(grammarAccess.getDinosaurAccess().getDinosaurKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getDinosaurAccess().getDinosaurKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__Group__0__Impl"


    // $ANTLR start "rule__Dinosaur__Group__1"
    // InternalDino.g:129:1: rule__Dinosaur__Group__1 : rule__Dinosaur__Group__1__Impl rule__Dinosaur__Group__2 ;
    public final void rule__Dinosaur__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:133:1: ( rule__Dinosaur__Group__1__Impl rule__Dinosaur__Group__2 )
            // InternalDino.g:134:2: rule__Dinosaur__Group__1__Impl rule__Dinosaur__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Dinosaur__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Dinosaur__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__Group__1"


    // $ANTLR start "rule__Dinosaur__Group__1__Impl"
    // InternalDino.g:141:1: rule__Dinosaur__Group__1__Impl : ( ( rule__Dinosaur__IdAssignment_1 ) ) ;
    public final void rule__Dinosaur__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:145:1: ( ( ( rule__Dinosaur__IdAssignment_1 ) ) )
            // InternalDino.g:146:1: ( ( rule__Dinosaur__IdAssignment_1 ) )
            {
            // InternalDino.g:146:1: ( ( rule__Dinosaur__IdAssignment_1 ) )
            // InternalDino.g:147:2: ( rule__Dinosaur__IdAssignment_1 )
            {
             before(grammarAccess.getDinosaurAccess().getIdAssignment_1()); 
            // InternalDino.g:148:2: ( rule__Dinosaur__IdAssignment_1 )
            // InternalDino.g:148:3: rule__Dinosaur__IdAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Dinosaur__IdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDinosaurAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__Group__1__Impl"


    // $ANTLR start "rule__Dinosaur__Group__2"
    // InternalDino.g:156:1: rule__Dinosaur__Group__2 : rule__Dinosaur__Group__2__Impl rule__Dinosaur__Group__3 ;
    public final void rule__Dinosaur__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:160:1: ( rule__Dinosaur__Group__2__Impl rule__Dinosaur__Group__3 )
            // InternalDino.g:161:2: rule__Dinosaur__Group__2__Impl rule__Dinosaur__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__Dinosaur__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Dinosaur__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__Group__2"


    // $ANTLR start "rule__Dinosaur__Group__2__Impl"
    // InternalDino.g:168:1: rule__Dinosaur__Group__2__Impl : ( ( rule__Dinosaur__NameAssignment_2 ) ) ;
    public final void rule__Dinosaur__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:172:1: ( ( ( rule__Dinosaur__NameAssignment_2 ) ) )
            // InternalDino.g:173:1: ( ( rule__Dinosaur__NameAssignment_2 ) )
            {
            // InternalDino.g:173:1: ( ( rule__Dinosaur__NameAssignment_2 ) )
            // InternalDino.g:174:2: ( rule__Dinosaur__NameAssignment_2 )
            {
             before(grammarAccess.getDinosaurAccess().getNameAssignment_2()); 
            // InternalDino.g:175:2: ( rule__Dinosaur__NameAssignment_2 )
            // InternalDino.g:175:3: rule__Dinosaur__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Dinosaur__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDinosaurAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__Group__2__Impl"


    // $ANTLR start "rule__Dinosaur__Group__3"
    // InternalDino.g:183:1: rule__Dinosaur__Group__3 : rule__Dinosaur__Group__3__Impl rule__Dinosaur__Group__4 ;
    public final void rule__Dinosaur__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:187:1: ( rule__Dinosaur__Group__3__Impl rule__Dinosaur__Group__4 )
            // InternalDino.g:188:2: rule__Dinosaur__Group__3__Impl rule__Dinosaur__Group__4
            {
            pushFollow(FOLLOW_7);
            rule__Dinosaur__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Dinosaur__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__Group__3"


    // $ANTLR start "rule__Dinosaur__Group__3__Impl"
    // InternalDino.g:195:1: rule__Dinosaur__Group__3__Impl : ( ( rule__Dinosaur__AgeAssignment_3 ) ) ;
    public final void rule__Dinosaur__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:199:1: ( ( ( rule__Dinosaur__AgeAssignment_3 ) ) )
            // InternalDino.g:200:1: ( ( rule__Dinosaur__AgeAssignment_3 ) )
            {
            // InternalDino.g:200:1: ( ( rule__Dinosaur__AgeAssignment_3 ) )
            // InternalDino.g:201:2: ( rule__Dinosaur__AgeAssignment_3 )
            {
             before(grammarAccess.getDinosaurAccess().getAgeAssignment_3()); 
            // InternalDino.g:202:2: ( rule__Dinosaur__AgeAssignment_3 )
            // InternalDino.g:202:3: rule__Dinosaur__AgeAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Dinosaur__AgeAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getDinosaurAccess().getAgeAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__Group__3__Impl"


    // $ANTLR start "rule__Dinosaur__Group__4"
    // InternalDino.g:210:1: rule__Dinosaur__Group__4 : rule__Dinosaur__Group__4__Impl ;
    public final void rule__Dinosaur__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:214:1: ( rule__Dinosaur__Group__4__Impl )
            // InternalDino.g:215:2: rule__Dinosaur__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Dinosaur__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__Group__4"


    // $ANTLR start "rule__Dinosaur__Group__4__Impl"
    // InternalDino.g:221:1: rule__Dinosaur__Group__4__Impl : ( ( rule__Dinosaur__Group_4__0 )? ) ;
    public final void rule__Dinosaur__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:225:1: ( ( ( rule__Dinosaur__Group_4__0 )? ) )
            // InternalDino.g:226:1: ( ( rule__Dinosaur__Group_4__0 )? )
            {
            // InternalDino.g:226:1: ( ( rule__Dinosaur__Group_4__0 )? )
            // InternalDino.g:227:2: ( rule__Dinosaur__Group_4__0 )?
            {
             before(grammarAccess.getDinosaurAccess().getGroup_4()); 
            // InternalDino.g:228:2: ( rule__Dinosaur__Group_4__0 )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==12) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalDino.g:228:3: rule__Dinosaur__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Dinosaur__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDinosaurAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__Group__4__Impl"


    // $ANTLR start "rule__Dinosaur__Group_4__0"
    // InternalDino.g:237:1: rule__Dinosaur__Group_4__0 : rule__Dinosaur__Group_4__0__Impl rule__Dinosaur__Group_4__1 ;
    public final void rule__Dinosaur__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:241:1: ( rule__Dinosaur__Group_4__0__Impl rule__Dinosaur__Group_4__1 )
            // InternalDino.g:242:2: rule__Dinosaur__Group_4__0__Impl rule__Dinosaur__Group_4__1
            {
            pushFollow(FOLLOW_4);
            rule__Dinosaur__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Dinosaur__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__Group_4__0"


    // $ANTLR start "rule__Dinosaur__Group_4__0__Impl"
    // InternalDino.g:249:1: rule__Dinosaur__Group_4__0__Impl : ( 'childOf' ) ;
    public final void rule__Dinosaur__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:253:1: ( ( 'childOf' ) )
            // InternalDino.g:254:1: ( 'childOf' )
            {
            // InternalDino.g:254:1: ( 'childOf' )
            // InternalDino.g:255:2: 'childOf'
            {
             before(grammarAccess.getDinosaurAccess().getChildOfKeyword_4_0()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getDinosaurAccess().getChildOfKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__Group_4__0__Impl"


    // $ANTLR start "rule__Dinosaur__Group_4__1"
    // InternalDino.g:264:1: rule__Dinosaur__Group_4__1 : rule__Dinosaur__Group_4__1__Impl ;
    public final void rule__Dinosaur__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:268:1: ( rule__Dinosaur__Group_4__1__Impl )
            // InternalDino.g:269:2: rule__Dinosaur__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Dinosaur__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__Group_4__1"


    // $ANTLR start "rule__Dinosaur__Group_4__1__Impl"
    // InternalDino.g:275:1: rule__Dinosaur__Group_4__1__Impl : ( ( rule__Dinosaur__ChildofAssignment_4_1 ) ) ;
    public final void rule__Dinosaur__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:279:1: ( ( ( rule__Dinosaur__ChildofAssignment_4_1 ) ) )
            // InternalDino.g:280:1: ( ( rule__Dinosaur__ChildofAssignment_4_1 ) )
            {
            // InternalDino.g:280:1: ( ( rule__Dinosaur__ChildofAssignment_4_1 ) )
            // InternalDino.g:281:2: ( rule__Dinosaur__ChildofAssignment_4_1 )
            {
             before(grammarAccess.getDinosaurAccess().getChildofAssignment_4_1()); 
            // InternalDino.g:282:2: ( rule__Dinosaur__ChildofAssignment_4_1 )
            // InternalDino.g:282:3: rule__Dinosaur__ChildofAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Dinosaur__ChildofAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getDinosaurAccess().getChildofAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__Group_4__1__Impl"


    // $ANTLR start "rule__Model__DinosaurAssignment"
    // InternalDino.g:291:1: rule__Model__DinosaurAssignment : ( ruleDinosaur ) ;
    public final void rule__Model__DinosaurAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:295:1: ( ( ruleDinosaur ) )
            // InternalDino.g:296:2: ( ruleDinosaur )
            {
            // InternalDino.g:296:2: ( ruleDinosaur )
            // InternalDino.g:297:3: ruleDinosaur
            {
             before(grammarAccess.getModelAccess().getDinosaurDinosaurParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleDinosaur();

            state._fsp--;

             after(grammarAccess.getModelAccess().getDinosaurDinosaurParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__DinosaurAssignment"


    // $ANTLR start "rule__Dinosaur__IdAssignment_1"
    // InternalDino.g:306:1: rule__Dinosaur__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__Dinosaur__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:310:1: ( ( RULE_ID ) )
            // InternalDino.g:311:2: ( RULE_ID )
            {
            // InternalDino.g:311:2: ( RULE_ID )
            // InternalDino.g:312:3: RULE_ID
            {
             before(grammarAccess.getDinosaurAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getDinosaurAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__IdAssignment_1"


    // $ANTLR start "rule__Dinosaur__NameAssignment_2"
    // InternalDino.g:321:1: rule__Dinosaur__NameAssignment_2 : ( RULE_STRING ) ;
    public final void rule__Dinosaur__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:325:1: ( ( RULE_STRING ) )
            // InternalDino.g:326:2: ( RULE_STRING )
            {
            // InternalDino.g:326:2: ( RULE_STRING )
            // InternalDino.g:327:3: RULE_STRING
            {
             before(grammarAccess.getDinosaurAccess().getNameSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getDinosaurAccess().getNameSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__NameAssignment_2"


    // $ANTLR start "rule__Dinosaur__AgeAssignment_3"
    // InternalDino.g:336:1: rule__Dinosaur__AgeAssignment_3 : ( RULE_INT ) ;
    public final void rule__Dinosaur__AgeAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:340:1: ( ( RULE_INT ) )
            // InternalDino.g:341:2: ( RULE_INT )
            {
            // InternalDino.g:341:2: ( RULE_INT )
            // InternalDino.g:342:3: RULE_INT
            {
             before(grammarAccess.getDinosaurAccess().getAgeINTTerminalRuleCall_3_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getDinosaurAccess().getAgeINTTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__AgeAssignment_3"


    // $ANTLR start "rule__Dinosaur__ChildofAssignment_4_1"
    // InternalDino.g:351:1: rule__Dinosaur__ChildofAssignment_4_1 : ( ( RULE_ID ) ) ;
    public final void rule__Dinosaur__ChildofAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDino.g:355:1: ( ( ( RULE_ID ) ) )
            // InternalDino.g:356:2: ( ( RULE_ID ) )
            {
            // InternalDino.g:356:2: ( ( RULE_ID ) )
            // InternalDino.g:357:3: ( RULE_ID )
            {
             before(grammarAccess.getDinosaurAccess().getChildofDinosaurCrossReference_4_1_0()); 
            // InternalDino.g:358:3: ( RULE_ID )
            // InternalDino.g:359:4: RULE_ID
            {
             before(grammarAccess.getDinosaurAccess().getChildofDinosaurIDTerminalRuleCall_4_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getDinosaurAccess().getChildofDinosaurIDTerminalRuleCall_4_1_0_1()); 

            }

             after(grammarAccess.getDinosaurAccess().getChildofDinosaurCrossReference_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dinosaur__ChildofAssignment_4_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000001000L});

}